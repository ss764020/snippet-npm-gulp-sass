# 0. 概要

* title:  SASSコンパイル自動実行
* author: y.ohta（https://auto-you-skit.com）
* create: 2016.05.04
* update: 2021.10.02
* rev.:   5.0.0

# 1. SASSコンパイル環境の構築方法

## (1-1) node.jsをインストール

#### (a) Windows環境の場合

下記よりダウンロード、インストール  
https://nodejs.org/ja/

#### (b) Linux環境の場合

下記実行（Redhat系を想定。Debian系ならaptコマンドでインストール。）

```
$ sudo yum install epel-release
$ sudo yum install nodejs npm --enablerepo=epel
```

## (1-2) npm及びgulpをインストール

下記コマンド実行

```
$ sudo npm install -g npm
$ sudo npm install -g gulp
```

## (1-3) SASSコンパイル実行に必要なパッケージインストール

#### (a) Windows環境の場合

sass_convert.bat  
をダブルクリックして実行
（インストール及びSASSの自動コンパイルが走る）  
  
※ 構築環境がWSL2であり**WSL2ファイルシステム上のディレクトリにて実行する場合**、Windows上のNode.jsを利用すると正常に動作しないため、batからwslコマンドでWSL上のNode.jsを実行するようにしている。よって、**WSL上でNode.jsをインストールしていないと動作しない**。なお、WSL上でのNode.jsのインストールにNVMを利用していることが前提としているため、**WSLにてNVMがインストールされていないと動作しない**。

#### (b) Linux環境の場合

下記コマンド実行

```
$ cd [PATH TO PROJECT]
$ npm install
```

# 2. SASSコンパイルの方法

## (2-1) プロジェクトディレクトリ以下にて下記コマンド実行

#### (a) Windows環境の場合

※ (1-3)の手順でbatファイルをすでに起動済みの場合は本手順は不要。SASSファイルに更新があるたびに自動更新がかかる

sass_convert.bat  
をダブルクリックして実行

#### (b) Linux環境の場合

下記コマンド実行

```
$ cd [PATH TO PROJECT]
$ npm run default
```
