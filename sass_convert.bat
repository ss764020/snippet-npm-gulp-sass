@echo off
@setlocal enabledelayedexpansion

echo %~dp0 | find /N "wsl" >NUL
if not ERRORLEVEL 1 (
  pushd %~dp0
  set WSLPATH=!CD:~2!
  set WSLPATH=!WSLPATH:\=/!
  echo =============================================================
  echo  - WSL FileSystem -
  echo  DIR: !WSLPATH!
  echo =============================================================
  echo;
  echo;
  call wsl cd !WSLPATH!; source ~/.nvm/nvm.sh; npm install; npm run default
) else (
  cd /d %~dp0
  echo =============================================================
  echo  - Windows FileSystem -
  echo  DIR: !CD!
  echo =============================================================
  echo;
  echo;
  echo -------------- install npm --------------
  echo;
  call npm install
  echo;
  echo;
  echo -------------- convert ^& watch sass --------------
  echo;
  call npm run default
)
