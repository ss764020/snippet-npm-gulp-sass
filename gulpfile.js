var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var notify  = require('gulp-notify')
var rename = require('gulp-rename');

var sass_paths = {
  'scss': './sass/',
  'css': './css/'
}

function buildStyles() {
  return gulp.src(sass_paths.scss + '**/*.scss')
  .pipe(plumber({
    errorHandler: notify.onError({
      title: "SASS compile error!",
      message: "Error: <%= error.message %>"
    })
  }))
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'compressed',
    errLogToConsole: false
  }))
  .pipe(autoprefixer())
  .pipe(rename({extname: '.min.css'})) // rename hoge.min.css
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(sass_paths.css))
}
exports.buildStyles = buildStyles;

// watch
function watch() {
  gulp.watch( sass_paths.scss + '**/*.scss', buildStyles);
}
exports.watch = watch;


// default
exports.default =　gulp.parallel(buildStyles, watch);